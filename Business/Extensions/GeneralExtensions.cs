﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;

namespace Nergard.EPi.QualityTools.Business.Extensions
{
    public static class GeneralExtensions
    {
        public static PageData GetPage(this ContentReference reference)
        {
            var locator = ServiceLocator.Current.GetInstance<IContentRepository>();
            return locator.Get<PageData>(reference);
        }
    }
}