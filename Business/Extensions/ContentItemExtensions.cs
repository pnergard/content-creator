﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using Nergard.EPi.QualityTools.Models;

namespace Nergard.EPi.QualityTools.Business.Extensions
{
    public static class ContentItemExtensions
    {
        public static string ContentTypeName (this ContentItem item)
        {
            var repository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            return repository.Load(item.ContentTypeID).LocalizedName;
        }
    }
}