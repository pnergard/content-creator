<%@ Page Language="c#" EnableViewState="true" Inherits="Nergard.EPi.QualityTools.Views.Pages.ContentCreatorPageTemplate" CodeBehind="ContentCreatorPageTemplate.aspx.cs" MasterPageFile="~/modules/Nergard.EPi.QualityTools/Views/MasterPages/Master.Master" %>

<%@ Import Namespace="Nergard.EPi.QualityTools.Models" %>
<%@ Import Namespace="Nergard.EPi.QualityTools.Business.Extensions" %>
<%@ Import Namespace="EPiServer.DataAbstraction" %>

<asp:Content ContentPlaceHolderID="Content" runat="server">

    <div class="jumbotron">
        <h1>Content creator</h1>
        <p>Content creator is a tool to quick and easy change content of pages in a web site.</p>
        <p>All settings is changed from forms mode, and you can set the starting point and drag and drop pages that will be used as "template pages" into a content area.</p>
        <p>For all chosen page types, Content creator get all pages and randomly replace the content with a "template page" for that type.</p>
        <p>The "template pages" are grouped by type and displayed below.</p>
    </div>

    <div class="jumbotron">
        <asp:Button ClientIDMode="Static" ID="btnGo" CssClass="btn" OnClientClick="if(!SetupIds()) {return false;};" UseSubmitBehavior="false" OnClick="btnGo_Click" runat="server" Text="GO!" />
    </div>

    <asp:Repeater ID="rptResult" runat="server">
        <HeaderTemplate>
                <div class="alert alert-warning">
                    <h2>Pages replaced:</h2>
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
        </HeaderTemplate>
        <ItemTemplate>

            <div>
                <strong><%#((KeyValuePair<string,int>)Container.DataItem).Key %></strong> <%#((KeyValuePair<string,int>)Container.DataItem).Value.ToString() %>
            </div>

        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>

    <asp:Repeater ID="rptContentPages" runat="server">
        <HeaderTemplate>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="alert alert-success" role="alert">
                        <input id="checkboxAll" type="checkbox" value="All" checked />All
                    </div>
                </div>
        </HeaderTemplate>

        <ItemTemplate>
            <div class="col-xs-12 col-md-4">
                <div class="alert alert-success" role="alert">
                    <input class="checkboxTypeGroup" type="checkbox" value="<%#((ContentItem)Container.DataItem).ContentTypeID %>" checked /><%#((ContentItem)Container.DataItem).ContentTypeName() %>
                </div>
                <asp:Repeater runat="server" DataSource="<%#((ContentItem)Container.DataItem).Items %>">
                    <ItemTemplate>
                        <a href="<%#((PageData)Container.DataItem).LinkURL %>" target="_blank"><%#((PageData)Container.DataItem).Name %></a>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </ItemTemplate>

        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>


</asp:Content>
