using System;
using EPiServer.DataAbstraction;
using EPiServer.Framework.DataAnnotations;
using Nergard.EPi.QualityTools.Models.Pages;

namespace Nergard.EPi.QualityTools.Views.Pages
{
    [TemplateDescriptor(Path = "~/modules/Nergard.EPi.QualityTools/Views/Pages/ContentCreatorPageTemplate.aspx")]
    public partial class ContentCreatorPageTemplate : EPiServer.TemplatePage<ContentCreatorPage>
    {
        public PropertyDefinition Proppen { get { return Page.GetDataItem() as PropertyDefinition; } }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            if (!IsPostBack)
            {
                rptContentPages.DataSource = CurrentPage.GetContentPages();
                rptContentPages.DataBind();
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            var pageTypeIDs = Request.Form["hiddenPageTypeIDs"];
            var dictionary = CurrentPage.CreateContent(pageTypeIDs);

            rptContentPages.DataSource = CurrentPage.GetContentPages();
            rptContentPages.DataBind();

            rptResult.DataSource = dictionary;
            rptResult.DataBind();

        }
    }
}