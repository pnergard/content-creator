﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EPiServer;

namespace Nergard.EPi.QualityTools.Interfaces
{
    public interface IPublishingContent
    {
        void PublishingContent(object sender, ContentEventArgs e);
    } 
}

