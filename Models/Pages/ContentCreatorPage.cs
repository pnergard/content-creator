﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.DataAnnotations;
using EPiServer.ServiceLocation;

namespace Nergard.EPi.QualityTools.Models.Pages
{
    [ContentType(DisplayName = "ContentCreator", GUID = "5b3d2d25-aaa9-4488-bbf9-b8706beebe95", Description = "")]
    public class ContentCreatorPage : PageData //, IPublishingContent
    {
        /*
                [CultureSpecific]
                [Display(
                    Name = "Main body",
                    Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual XhtmlString MainBody { get; set; }
         */

        #region EPiServer-properties
        public virtual ContentReference StartingPoint { get; set; }
        public virtual ContentArea TemplatePages { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Get all template pages and group them by type
        /// </summary>
        /// <returns></returns>
        public List<ContentItem> GetContentPages()
        {
            Dictionary<int, List<IContent>> PageTypesIdListDictionary = new Dictionary<int, List<IContent>>();
            List<ContentItem> returnList = new List<ContentItem>();

            if (TemplatePages.Items != null)
            {
                List<IContent> allContent = new List<IContent>();

                foreach (var item in TemplatePages.Items)
                {
                    allContent.Add(item.GetContent());
                }

                PageTypesIdListDictionary = allContent
                    .GroupBy(o => o.ContentTypeID)
                    .ToDictionary(g => g.Key, g => g.ToList());

                var repository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();

                foreach (var content in PageTypesIdListDictionary)
                {
                    returnList.Add(new ContentItem { ContentTypeID = content.Key, Items = content.Value.Cast<PageData>().ToList() });
                }
            }

            return returnList;
        }

        /// <summary>
        /// Create new page versions and replacing the data from the randomly chosen template page
        /// </summary>
        /// <param name="chosenContentTypeIDs"></param>
        /// <returns></returns>
        public Dictionary<string, int> CreateContent(string chosenContentTypeIDs)
        {
            Random random = new Random();
            var contentTypeIDs = chosenContentTypeIDs.Split(',');
            var locator = ServiceLocator.Current.GetInstance<IContentRepository>();
            var typelocator = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            Dictionary<string, int> resultDictionary = new Dictionary<string, int>();
            int cnt;

            foreach (var typeID in contentTypeIDs)
            {
                PropertyCriteria criteria = new PropertyCriteria()
                {
                    Name = "PageTypeID",
                    Condition = EPiServer.Filters.CompareCondition.Equal,
                    Required = true,
                    Type = PropertyDataType.PageType,
                    Value = typeID

                };

                PropertyCriteriaCollection criterias = new PropertyCriteriaCollection();
                criterias.Add(criteria);

                var pagesToProcess = DataFactory.Instance.FindPagesWithCriteria(StartingPoint.ToPageReference(), criterias);
                var templatePages = GetContentPages().FirstOrDefault(x=> x.ContentTypeID.ToString() == typeID).Items;
                var templatePagesCount = templatePages.Count() - 1;
                var type = typelocator.Load(Int32.Parse(typeID));
                cnt = 0;

                foreach (var page in pagesToProcess)
                {
                    cnt++;

                    var template = templatePages[random.Next(0,templatePagesCount)];
                    
                    if (page.ContentLink == template.ContentLink) continue;

                    var writableCopy = page.CreateWritableClone();
             
                    foreach (var prop in type.PropertyDefinitions)
                    {
                        if (template[prop.Name] != null)
                            writableCopy.Property[prop.Name].Value = template.Property[prop.Name].Value;
                        else
                            writableCopy.Property[prop.Name].Value = null;
                    }

                    locator.Save(writableCopy, SaveAction.Publish, EPiServer.Security.AccessLevel.NoAccess);

                    resultDictionary.Add(type.LocalizedName, cnt);
                }
            }

            return resultDictionary;
        }


        #endregion

    }
}