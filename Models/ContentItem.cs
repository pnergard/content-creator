﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Nergard.EPi.QualityTools.Models
{
    public class ContentItem
    {
        public int ContentTypeID { get; set; }
        public List<PageData> Items { get; set; }
    }
}